import React from "react";

interface MyButtonProps {
    title: string;
    disabled: boolean;
}
function MyButton({ title, disabled }: MyButtonProps) {
    return (
        <div className={ disabled ? 'disabled-button' : '' }>{title}</div>
    );
}

export default MyButton;
